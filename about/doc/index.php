<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle(" Документы");
?>

  <p> Создание образовательного пространства в ДОУ во многом определяется целесообразной организацией образовательного процесса. Немаловажным при этом является соблюдение нормативно-правовых актов, регламентирующих деятельность педагога и всего педагогического коллектива при организации воспитания и обучения детей.
Нормативно-правовой акт - закон, кодекс, постановление, инструкция и другое властное предписание государственных органов, которое устанавливает, изменяет или отменяет нормы права. Предписания нормативно-правовых актов носят общий характер и направлены на регулирование определенного вида общественных отношений. В Российской Федерации и европейских государствах нормативно-правовой акт является основным источником права (Глоссарий.ру).</p>

        <ul class="ul1">
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="ustav.php"><span>Устав</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="litsenziya.php"><span>Лицензия на образовательную деятельность</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="litsenziya-na-meditsinskuyu-deyatelnost.php"><span>Лицензия на медицинскую деятельность</span></a></li>
          <!--li class="clearfix"><i class="fa fa-file-word-o"></i><a href="svidetelstvo-o-gosudarstvennoy-akreditatsii.php"><span>Свидетельство о государственной акредитации</span></a></li-->
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="plan-fkhd-na-2015-god.php"><span>План ФХД на 2016-2017 годы</span></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="pravila-vnutrennego-rasporyadka-vospitannikov.php"><span>Правила внутреннего распорядка воспитанников</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="pravila-vnutrennego-trudovogo-rasporyadka.php"><span>Правила внутреннего трудового распорядка</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="kollektivnyy-dogovor.php"><span>Коллективный договор</span></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="otchet-o-rezultatakh-samoobsledovaniya.php"><span>Отчет о результатах самообследования</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="predpisaniya.php"><span>Предписания</span></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="otchet-ob-ispolnenii-predpisaniy.php"><span>Отчет об исполнении предписаний</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="pravila-priema-detey-v-dou.php"><span>Правила приема детей в ДОУ</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="rezhim-zanyatiy-vospitannikov.php"><span>Режим занятий воспитанников</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><a href="polozhenie-o-poryadke-i-osnovaniyakh-perevoda.php"><span>Положение о порядке и основаниях перевода, отчисления и восстановления воспитанников</span></a></li>
          <li class="clearfix"><i class="fa fa-file-word-o"></i><span><a href="prikaz-o-zachislenii-detey-v-dou-na-2015-g.php">Приказ о зачислении в МБДОУ №237</span></a></li>
		  <li class="clearfix"><i class="fa fa-file-word-o"></i><span><a href="monitoring-zarabotnoy-platy.php
">Мониторинг заработной платы</span></a></li>
        </ul>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>