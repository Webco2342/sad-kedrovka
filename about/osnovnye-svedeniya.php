<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Основные сведения");
?><p align="JUSTIFY">
	 Полное наименование учреждения:
</p>
<p align="JUSTIFY">
	 Муниципальное бюджетной дошкольное образовательное учреждение &nbsp;№ 237 "Детский сад общеразвивающего вида с приоритетным осуществлением &nbsp;деятельности по физическому направлению развития воспитанников"
</p>
<p align="JUSTIFY">
	 Краткое наименование: МБДОУ№237 "Детский сад общеразвивающего вида"
</p>
<p align="JUSTIFY">
	 Организационнно-правовая форма - муниципальная
</p>
<p align="JUSTIFY">
	 Адрес:&nbsp;650903&nbsp; Россия, город Кемерово,&nbsp; Греческая деревня 157 а.
</p>
<p align="JUSTIFY">
 <a href="http://clck.yandex.ru/redir/dv/*data=url%3Dhttp%253A%252F%252Fterem230.ucoz.ru%252Findex%252Fosnovnye_svedenija%252F0-246%26ts%3D1454220271%26uid%3D361016501450589924&sign=eb160c8338ba51807a2cf91dd8d4e413&keyno=1" target="_blank">Телефон</a>:&nbsp;69-17-90;
</p>
<p align="JUSTIFY">
	 Факс:&nbsp;69-17-90
</p>
<p align="JUSTIFY">
	 Е-mail:&nbsp; <a href="mailto:rambier.ru@rambler.ru">rambier.ru@rambler.ru</a>
</p>
<p align="JUSTIFY">
	 Год основания:&nbsp;1995 г.
</p>
<p align="JUSTIFY">
	 Учредители:&nbsp;&nbsp;муниципальное образование города Кемерово
</p>
<p>
	 Функции и полномочия учредителя осуществляются
</p>
<p>
	 Управлением образования администрации города Кемерово
</p>
<p>
	 Почтовый адрес: Россия, 650000, город Кемерово пр. Советский, 54 &nbsp; тел. 8-(3842) 36-46-19
</p>
<p>
	 Комитет по управлению муниципальным имуществом города Кемерово
</p>
<p>
	 почтовый адрес: Россия, 650000, город Кемерово, ул. Притомская набережная, 7
</p>
<p>
 <a href="http://clck.yandex.ru/redir/dv/*data=url%3Dhttp%253A%252F%252Fwww.kumi-kemerovo.ru%252F%26ts%3D1454220271%26uid%3D361016501450589924&sign=4f6ab063ced9bb4c4a0f0b4a3de7a7a1&keyno=1" target="_blank">http://www.kumi-kemerovo.ru/&nbsp;</a>тел. 8-(384-2) 36-81-71
</p>
<p align="JUSTIFY">
	 Учредительные документы:
</p>
<ul>
	<li>
	<p align="JUSTIFY">
		 Лицензия &nbsp;на осуществление &nbsp;образовательной деятельности&nbsp;&nbsp;от 23/011/2012&nbsp;г. серия А&nbsp;№ 0003051 регистрационный № 13258
	</p>
 </li>
	<li>
	<p align="JUSTIFY">
		 Лицензия&nbsp;на осуществление медицинской деятельности&nbsp;&nbsp;от 23/11/2010 г. регистрационный № ФС - 42 - 01 - 001500
	</p>
 </li>
	<li>
	<p align="JUSTIFY">
		 Свидетельство &nbsp;о государственной &nbsp;аккредитации
	</p>
 </li>
</ul>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 Режим работы:
</p>
<p align="JUSTIFY">
	 ДОУ работает по пятидневной рабочей неделе с двумя выходными днями (суббота, воскресенье). Режим работы - 12 часов в день (с 7.00.до 19.00 часов).
</p>
<p align="JUSTIFY">
	 Правила приёма&nbsp;&nbsp;в ДОУ:
</p>
<p align="JUSTIFY">
	 Приём детей в ДОУ осуществляется&nbsp;&nbsp;при личном обращении&nbsp;&nbsp;к&nbsp;&nbsp;заведующей&nbsp;&nbsp;с предъявлением&nbsp;&nbsp;следующих документов:&nbsp;&nbsp;паспорт&nbsp;&nbsp;родителей, свидетельство о рождении&nbsp;&nbsp;ребёнка, справка о льготах (если таковые имеются), или&nbsp;&nbsp;само регистрация на сайте:<a href="http://clck.yandex.ru/redir/dv/*data=url%3Dhttps%253A%252F%252Fcabinet.ruobr.ru%252Flogin%252F%26ts%3D1454220271%26uid%3D361016501450589924&sign=9323c83afbda085a5d22f007f4c8ad5d&keyno=1" target="_blank">https://cabinet.ruobr.ru/login/</a>
</p>
<p align="JUSTIFY">
	 Отношения учреждения с родителями определяются договором о сотрудничестве, который заключается при приеме ребенка в ДОУ.&nbsp;
</p>
<p align="JUSTIFY">
	 Списочный состав &nbsp;ДОУ &nbsp;на &nbsp;1 &nbsp;сентября &nbsp;2015&nbsp; года составляет 57 &nbsp;воспитанников.
</p>
<p align="JUSTIFY">
</p>
<table cellspacing="1" class="table-striped table-bordered">
 <colgroup><col><col></colgroup>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			 возрастная группа&nbsp;
		</p>
	</td>
	<td>
		<p align="CENTER">
			 списочный состав
		</p>
	</td>
</tr>
<tr>
	<td>
		<p align="CENTER">
			 1 младшая группа&nbsp;
		</p>
		<p align="CENTER">
			 /1,5-3 года/
		</p>
	</td>
	<td>
		<p align="CENTER">
			 16
		</p>
	</td>
</tr>
<tr>
	<td>
		<p align="CENTER">
			 Разновозрастная группа «Пчелки»
		</p>
	</td>
	<td>
		<p align="CENTER">
			 21
		</p>
	</td>
</tr>
<tr>
	<td>
		<p>
			 Разновозрастная гркппа «Звездочки»
		</p>
	</td>
	<td>
		<p align="CENTER">
			 20
		</p>
	</td>
</tr>
</tbody>
</table>
<p align="JUSTIFY">
</p>
<p align="JUSTIFY">
	 Образовательная деятельность в ДОУ осуществляется на государственном языке Российской Федерации.
</p>
 <br>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>