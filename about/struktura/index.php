<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Структура и органы управления образовательной организацией");
?>

<div id="fix" class="fix">
<div class="row">
  <div class="rs-0"></div>
</div>
<div class="row">
  <div class="col-xs-4 col-xs-offset-4"><h2  class="btn btn-danger stuctura_titl bl0">Заведующий ДОУ</h2></div>
</div>

<div class="row">
  <div class="col-xs-4 col-xs-offset-4 rs-1"></div>
</div>

<div class="row">
  <div class="col-xs-3 col-xs-offset-2"><h2  class="btn btn-warning stuctura_titl bl11">Общее собрание МБДОУ<p>(Положение см. Устав)</p></h2></div>
  <div class="col-xs-3 col-xs-offset-2"><h2  class="btn btn-warning stuctura_titl bl12">Педагогический совет<p>(Положение см. Устав)</p></h2></div>
</div>
<div class="row">
  <div class="col-xs-8 col-xs-offset-2 rs-1"></div>
</div>

<div class="row">
  <div class="col-xs-3 "><h2  class="btn btn-info stuctura_titl bl21">Родительский Комитет<p>(Положение см. Устав)</p></h2></div>
  <div class="col-xs-3 col-xs-offset-6"><h2  class="btn btn-info stuctura_titl bl22">Общее продительское собрание<p>(Положение см. Устав)</p></h2></div>
</div>

<div class="row">
  <div class="col-xs-8 col-xs-offset-2 rs-1"></div>
</div>

<div class="row">
  <div class="col-xs-4"><h2  class="btn btn-primary stuctura_titl bl31">Старший воспитатель</h2></div>
  <div class="col-xs-4 "><h2  class="btn btn-default stuctura_titl bl32">Медицинский персонал</h2></div>
  <div class="col-xs-4 "><h2  class="btn btn-success stuctura_titl bl33">Завхоз</h2></div>
</div>


<div class="row">
  <div class="col-xs-2 col-xs-offset-2 rs-1"></div>
</div>

<div class="row">
  <div class="col-xs-3 "><h2  class="btn btn-primary stuctura_titl bl41">Педагог психолог</h2></div>
  <div class="col-xs-3 "><h2  class="btn btn-primary stuctura_titl bl42">Воспитатели</h2></div>
  <div class="col-xs-5 col-xs-offset-1"><h2  class="btn btn-success stuctura_titl bl43">Учебно-вспомагательный и обслуживающий персонал</h2></div> 
</div>

<div class="row">
  <div class="col-xs-1 col-xs-offset-3 rs-1"></div>
</div>

<div class="row">
  <div class="col-xs-5"><h2  class="btn btn-primary stuctura_titl bl51">Инструктор по фмз. культуре и плаванью</h2></div>
</div>
</div>

<script type='text/javascript'>
 // Example 3: Initialize
 var arrowsDrawer1 = $cArrows('#fix', { render:{lineWidth: 2, strokeStyle: '#d43f3a'}});
 arrowsDrawer1.arrow('.bl0', '.bl11', {arrow: {connectionType: 'side', sideFrom: 'left', sideTo: 'top'}});
 arrowsDrawer1.arrow('.bl0', '.bl12', {arrow: {connectionType: 'side', sideFrom: 'right', sideTo: 'top'}});
 arrowsDrawer1.arrow('.bl0', '.bl21', {arrow: {connectionType: 'side', sideFrom: 'bottom', sideTo: 'right'}});
 arrowsDrawer1.arrow('.bl0', '.bl22', {arrow: {connectionType: 'side', sideFrom: 'bottom', sideTo: 'left'}});
 arrowsDrawer1.arrow('.bl0', '.bl31', {arrow: {connectionType: 'side', sideFrom: 'bottom', sideTo: 'top'}});
 arrowsDrawer1.arrow('.bl0', '.bl32');
 arrowsDrawer1.arrow('.bl0', '.bl33', {arrow: {connectionType: 'side', sideFrom: 'bottom', sideTo: 'top'}});
 arrowsDrawer1.arrow('.bl32','.bl31', {render: {strokeStyle:'#333'}});
 arrowsDrawer1.arrow('.bl32','.bl33', {render: {strokeStyle:'#333'}});
 arrowsDrawer1.arrow('.bl32','.bl41', {render: {strokeStyle:'#333'}});
 arrowsDrawer1.arrow('.bl32','.bl42', {render: {strokeStyle:'#333'}});
 arrowsDrawer1.arrow('.bl32','.bl43', {render: {strokeStyle:'#333'}});
 arrowsDrawer1.arrow('.bl32','.bl51', {render: {strokeStyle:'#333'}});
 arrowsDrawer1.arrow('.bl31','.bl41', {render: {strokeStyle:'#357ebd'}});
 arrowsDrawer1.arrow('.bl31','.bl42', {render: {strokeStyle:'#357ebd'}});
 arrowsDrawer1.arrow('.bl31','.bl51', {render: {strokeStyle:'#357ebd'}});
 arrowsDrawer1.arrow('.bl33','.bl43', {render: {strokeStyle:'#4cae4c'}});
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>