<?
$aMenuLinks = Array(
	Array(
		"Основные сведения", 
		"/about/osnovnye-svedeniya.php", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Структура и органы управления образовательной организацией", 
		"/about/struktura/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Документы", 
		"/about/doc/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Образование", 
		"/about/obrazovanie/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Образовательные стандарты (ФГОС ДО)", 
		"/about/obrazovatelnye-standarty/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Руководство. Педагогический состав", 
		"/about/rukovodstvo-pedagogicheskiy-sostav/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Материально-техническое обеспечение и оснащенность образовательного процесса", 
		"/about/obespechenie/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Платные образовательные услуги", 
		"/about/platnye-obrazovatelnye-uslugi/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Финансово-хозяйственная деятельность", 
		"/about/finansovo-khozyaystvennaya/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Мониторинг заработной платы", 
		"/about/doc/monitoring-zarabotnoy-platy.php", 
		Array(), 
		Array(), 
		"" 
	),	

	
	Array(
		"Вакантные  места для приема (перевода)", 
		"/about/vacancies/", 
		Array(), 
		Array(), 
		"" 
	)
);
?>