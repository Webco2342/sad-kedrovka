<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сведения о ДОУ");
?>



        <ul class="ul1">
          <li class="clearfix"><i class="fa fa-university"></i><span><a href="/about/osnovnye-svedeniya.php">Основные сведения</a></span></li>
          <li class="clearfix"><i class="fa fa-th-large"></i><span><a href="/about/struktura/">Структура и органы управления образовательной организацией</a></span></li>
          <li class="clearfix"><i class="fa fa-file-text"></i><span><a href="/about/doc/">Документы</a></span></li>
          <li class="clearfix"><i class="fa fa-graduation-cap"></i><span><a href="/about/obrazovanie/">Образование</a></span></li>
          <li class="clearfix"><i class="fa fa-suitcase"></i><span><a href="/about/obrazovatelnye-standarty/">Образовательные стандарты (ФГОС ДО)</a></span></li>
          <li class="clearfix"><i class="fa fa-users"></i><span><a href="/about/rukovodstvo-pedagogicheskiy-sostav/">Руководство. Педагогический состав</a></span></li>
          <li class="clearfix"><i class="fa fa-cogs"></i><span><a href="/about/obespechenie/">Материально-техническое обеспечение и оснащенность образовательного процесса</a></span></li>
          <li class="clearfix"><i class="fa fa-money"></i><span><a href="/about/platnye-obrazovatelnye-uslugi/">Платные образовательные услуги</a></span></li>
          <li class="clearfix"><i class="fa fa-rub"></i><span><a href="/about/finansovo-khozyaystvennaya/">Финансово-хозяйственная деятельность</a></span></li>
          <li class="clearfix"><i class="fa fa-child"></i><span><a href="/about/vacancies/">Вакантные  места для приема (перевода)</a></span></li>
        </ul>







<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>