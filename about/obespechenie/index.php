<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Материально-техническое обеспечение и оснащенность образовательного процесса");
?><p align="CENTER">
	Материально – техническое обеспечение
</p>
<p align="CENTER">
	<br>
</p>
<p align="CENTER">
	Помещения для физкультурно- оздоровительной работы с детьми
</p>
<p>
	<br>
</p>
<p>
	физкультурный зал
</p>
<p>
	бассейн ( в здании спорткомплекса)
</p>
<p>
	спортивная площадка на территории детского сада
</p>
<p>
	центр двигательной активности в каждой группе
</p>
<p>
	музыкальный зал
</p>
<p>
	зимний сад
</p>
<p>
	<br>
</p>
<p align="CENTER">
	Медицинский блок
</p>
<p>
	<br>
</p>
<p>
	Медицинский кабинет
</p>
<p>
	Процедурный кабинет
</p>
<p>
	Изолятр
</p>
<p>
	<br>
</p>
<p>
	<br>
</p>
<p align="CENTER">
	Организация предметно – развивающей среды&nbsp; МДОУ
</p>
<table cellspacing="0" class="table-striped table-bordered table-hover ">
<colgroup><col><col><col><col></colgroup>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			№
		</p>
	</td>
	<td>
		<p align="CENTER">
			Помещения
		</p>
	</td>
	<td>
		<p align="CENTER">
			Оборудование
		</p>
	</td>
	<td>
		<p align="CENTER">
			Мероприятия
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			1.
		</p>
	</td>
	<td>
		<p>
			Групповые помещения
		</p>
		<p>
		</p>
	</td>
	<td>
		<p align="CENTER">
			Игровое, учебное
		</p>
		<p align="CENTER">
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Организация&nbsp; ОД
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Организация игровой деятельности
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			2.
		</p>
	</td>
	<td>
		<p>
			Спортивный зал
		</p>
	</td>
	<td>
		<p align="CENTER">
			Спортивное
		</p>
		<p align="CENTER">
			<br>
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Утренняя гимнастика
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Физкультурные занятия
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Спортивные развлечения, соревнования
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Индивидуальная работа по усвоению физических навыков и умений
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Занятия по коррекции плоскостопия и нарушению осанки
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			3.
		</p>
		<p align="CENTER">
		</p>
	</td>
	<td>
		<p>
			Бассейн
		</p>
	</td>
	<td>
		<p align="CENTER">
			для обучения детей плаванию
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Физкультурные занятия по обучению детей плаванию
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Спортивные праздники и развлечения
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			4
		</p>
		<p align="CENTER">
		</p>
	</td>
	<td>
		<p>
			Спортивная площадка ДОУ
		</p>
	</td>
	<td>
		<p align="CENTER">
			шведские стенки, спортивные комплексы
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Утренняя гимнастика на воздухе
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Физкультурные занятия на воздухе
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Спортивные праздники и соревнования на воздухе
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			5.
		</p>
		<p align="CENTER">
		</p>
	</td>
	<td>
		<p>
			Музыкальный зал
		</p>
	</td>
	<td>
		<p align="CENTER">
			Пианино, музыкальный центр, (диски, кассеты)
		</p>
		<p align="CENTER">
			музыкальные инструменты, игрушки.
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Музыкальные занятия
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Индивидуальная работа по закреплению танцевальных движений
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Спектакли
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Развлечения, праздники
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			6.
		</p>
	</td>
	<td>
		<p>
			Медицинский кабинет
		</p>
		<p>
			(кабинет медицинской сестры, прививочный, изолятор)
		</p>
	</td>
	<td>
		<p align="CENTER">
			Документация, лекарственные препараты, оборудование
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Осмотр детей
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Антропометрия
		</p>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Вакцинация детей
		</p>
		<p>
		</p>
		<p>
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			7.
		</p>
	</td>
	<td>
		<p>
			Пищеблок
		</p>
		<p>
		</p>
	</td>
	<td>
		<p align="CENTER">
			Оборудование, посуда, продукты
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Приготовление пищи, подготовка и раздача пищи
		</p>
	</td>
</tr>
</tbody>
<tbody>
<tr>
	<td>
		<p align="CENTER">
			8.
		</p>
	</td>
	<td>
		<p>
			Прачечная
		</p>
	</td>
	<td>
		<p align="CENTER">
			Оборудование, белье
		</p>
	</td>
	<td>
		<p>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Стирка, сушка и глажение белья
		</p>
	</td>
</tr>
<tr>
	<td>
		<p align="CENTER">
			<br>
			<br>
		</p>
		<p align="CENTER">
			9.
		</p>
	</td>
	<td>
		<p>
			<br>
			<br>
		</p>
		<p>
			Зимний сад, живой уголок
		</p>
	</td>
	<td>
		<p align="CENTER">
			<br>
			<br>
		</p>
		<p>
			Растения, рыбы, птицы.
		</p>
	</td>
	<td>
		<p>
			<br>
			<br>
		</p>
		<p>
			Занятия по экологии.
		</p>
	</td>
</tr>
<tr>
	<td>
		<p align="CENTER">
			<br>
		</p>
	</td>
	<td>
		<p>
			<br>
		</p>
	</td>
	<td>
		<p align="CENTER">
			<br>
		</p>
	</td>
	<td>
		<p>
			<br>
		</p>
	</td>
</tr>
</tbody>
</table>
<p align="CENTER">
	<br>
</p>
<br>
<br>
<br>
<br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>