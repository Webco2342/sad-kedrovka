<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Полезные ссылки");
?><table class="table-striped table-hover">
<tbody>
<tr>
	<td>
 <a href="http://www.edu.ru/">http://www.edu.ru/</a>
	</td>
	<td>
		 федеральный портал &nbsp;российского &nbsp;образования
	</td>
</tr>
<tr>
	<td>
 <a href="http://xn--80abucjiibhv9a.xn--p1ai/">http://xn--80abucjiibhv9a.xn--p1ai/</a>
	</td>
	<td>
		 Министерство &nbsp;образования &nbsp;и науки &nbsp;РФ
	</td>
</tr>
<tr>
	<td>
 <a href="http://www.fcior.edu.ru/">http://www.fcior.edu.ru/</a>
	</td>
	<td>
		 министерство образования и &nbsp;науки российской федерации&nbsp;<br>
		 федеральный центр &nbsp;информационно-образовательных ресурсов
	</td>
</tr>
<tr>
	<td>
 <a href="http://www.zakonrf.info/">http://www.zakonrf.info/</a>
	</td>
	<td>
		 кодексы и законы РФ
	</td>
</tr>
<tr>
	<td>
 <a href="http://www.gosuslugi.ru/">http://www.gosuslugi.ru/</a>
	</td>
	<td>
		 портал государственных услуг
	</td>
</tr>
<tr>
	<td>
 <a href="http://www.ako.ru/elprav/pgu.asp">http://www.ako.ru/elprav/pgu.asp</a>
	</td>
	<td>
		 регистрация на портале госуслуг раздел"Электронное правительство"
	</td>
</tr>
<tr>
	<td>
 <a href="http://xn--42-6kcadhwnl3cfdx.xn--p1ai/">http://xn--42-6kcadhwnl3cfdx.xn--p1ai/</a>
	</td>
	<td>
		 департамент образования и науки кемеровской области
	</td>
</tr>
<tr>
	<td>
 <a href="http://window.edu.ru/">http://window.edu.ru/</a>
	</td>
	<td>
		 единое окно доступа к образовательным ресурсам
	</td>
</tr>
<tr>
	<td>
		 &nbsp;<a href="http://www.kostyor.ru/">http://www.kostyor.ru/ &nbsp; &nbsp;</a><br>
 <a href="http://lukoshko.net/">http://lukoshko.net/</a>&nbsp;
	</td>
	<td>
		 &nbsp;детская литература&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
	</td>
</tr>
<tr>
	<td>
 <a href="http://doshkolnik.ru/">http://doshkolnik.ru/&nbsp;</a><a href="http://www.babyblog.ru/">http://www.babyblog.ru/</a>
	</td>
	<td>
		<p>
			 методический материал для воспитанников<br>
			 педагогов, родителей
		</p>
	</td>
</tr>
<tr>
	<td>
 <a href="http://www.deti-pogodki.ru/">http://www.deti-pogodki.ru/</a>
	</td>
	<td>
		 семейный информационный портал
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://www.raskraska.ru/">http://www.raskraska.ru/</a>
	</td>
	<td colspan="1">
		 раскраски, картинки, рисунки для детей
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://detstvo.ru/">http://detstvo.ru/</a><br>
 <a href="http://www.kindereducation.com/">http://www.kindereducation.com/</a><br>
 <a href="http://www.detskiysad.ru/">http://www.detskiysad.ru/</a>
	</td>
	<td colspan="1">
		 сайты &nbsp;для детей и их родителей
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://www.detstvo-press.ru/">http://www.detstvo-press.ru/</a>
	</td>
	<td colspan="1">
		 дошкольное образование /методические пособия, наглядный материал и мн.другое/
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://edu.rin.ru/preschool/index.html">http://edu.rin.ru/preschool/index.html</a>
	</td>
	<td colspan="1">
		 дошкольное образование
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://talant.spb.ru/">http://talant.spb.ru/</a>
	</td>
	<td colspan="1">
		 созидание талантов общество раннего обучения
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://azps.ru/baby/index.html">http://azps.ru/baby/index.html</a>
	</td>
	<td colspan="1">
		 до и после трёх
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://wunderkinder.narod.ru/">http://wunderkinder.narod.ru/</a>
	</td>
	<td colspan="1">
		 материал необходимый для развития и обучения малышей
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://nmcmosc.ucoz.ru/">http://nmcmosc.ucoz.ru/</a>
	</td>
	<td colspan="1">
		 ребёнок и взрослый: один мир
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://tanja-k.chat.ru/">http://tanja-k.chat.ru/</a><br>
 <a href="http://www.detsadd.narod.ru/">http://www.detsadd.narod.ru/</a>
	</td>
	<td colspan="1">
		 материал для специалистов ДОУ
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://pedmir.ru/index.php">http://pedmir.ru/index.php</a>
	</td>
	<td colspan="1">
		 сайт для педагогов&nbsp;
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://nmc-kemerovo.ucoz.ru/">http://nmc-kemerovo.ucoz.ru/</a>
	</td>
	<td colspan="1">
		 научно методический центр г.Кемерово
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://portfolioprosto.jimdo.com/">http://portfolioprosto.jimdo.com/</a>
	</td>
	<td colspan="1">
		 как оформить портфолио
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://dk42.ru/">http://dk42.ru/</a>
	</td>
	<td colspan="1">
		 редакция дошколёнок Кузбасса
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://igraem.pro/">http://igraem.pro/</a>
	</td>
	<td colspan="1">
		 детские игры онлайн
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://www.artplot.ru/">http://www.artplot.ru/</a>
	</td>
	<td colspan="1">
		 интернет журнал &nbsp;
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://www.doumarx.ru/">http://www.doumarx.ru/</a>
	</td>
	<td colspan="1">
		 образовательный салон: в помощь педагогам ДОУ
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://www.school-collection.edu.ru/">http://www.school-collection.edu.ru/</a>
	</td>
	<td colspan="1">
		 единая коллекция цифровых образовательных ресурсов
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://mpado.ru/">http://mpado.ru</a>
	</td>
	<td colspan="1">
		 единый личный кабинет МПАДО
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://xn--80aalcbc2bocdadlpp9nfk.xn--d1acj3b/personalnye_dannye/">http://персональныеданные.дети/personalnye_dannye/</a>
	</td>
	<td colspan="1">
		 отдельный образовательный детский сайт для педагогов, детей и их родителей
	</td>
</tr>
<tr>
	<td colspan="1">
 <a href="http://www.kremlinrus.ru/news/165/43216/">http://www.kremlinrus.ru/news/165/43216/</a>
	</td>
	<td colspan="1">
		 Новостной образовательный реестр субъектов РФ "Общее образование"&nbsp;
	</td>
</tr>
</tbody>
</table><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>