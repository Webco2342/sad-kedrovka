<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$frame = $this->createFrame("slide", false)->begin();
?>

      <div id="slider_wrapper">
        <div id="slider">
          <div id="flexslider">
            <ul class="slides clearfix">		
               <?foreach ($arResult["ITEMS"] as $key=>$arItems): ?>	

             <li>
                <img src="<?=CFile::GetPath ($arItems["PREVIEW_PICTURE"])?>" alt="<?=htmlspecialchars_decode($arItems["NAME"])?>">
                <div class="flex-caption">
                  <div class="flex-caption_inner container">
                    <div class="txt1"><?=htmlspecialchars_decode($arItems["NAME"])?></div>
                    <div class="txt2"><?=htmlspecialchars_decode($arItems["PREVIEW_TEXT"])?></div>
                    <div class="txt3"><a href="<?=$arItems["CODE"]?>" class="btn-default btn0">Подробнее</a></div>
                  </div>
                </div>
              </li>
			   
			   

				<?endforeach;?>
            </ul>
          </div>
        </div>
      </div>
			
		
	<?$frame->end();?>