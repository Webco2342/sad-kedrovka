<?if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)die();
IncludeTemplateLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/templates/".SITE_TEMPLATE_ID."/header.php");
$curPage = $APPLICATION->GetCurPage(true);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<meta name='yandex-verification' content='64832c1068326138' />
	<head>
	<?$APPLICATION->ShowHead();
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/bootstrap.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/font-awesome.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/flexslider.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/animate.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/isotope.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css"); 
	$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/touchTouch.css"); 	
	
	CJSCore::Init(array("popup"));
	CJSCore::Init(array('date'));
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery-migrate-1.2.1.min.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.easing.1.3.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/superfish.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.parallax-1.1.3.resize.js");
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/SmoothScroll.js");		
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.appear.js");		
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.flexslider.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.caroufredsel.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.touchSwipe.min.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.ui.totop.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/touchTouch.jquery.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.isotope.min.js");		
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/script.js");	
	$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/arrows.1.0.0.js");

	?>
		<script>
		$(document).ready(function() {
		  //

		  //  carouFredSel team
		  $('#team .carousel.main ul').carouFredSel({
			auto: {
			  timeoutDuration: 8000
			},
			responsive: true,
			prev: '.team_prev',
			next: '.team_next',
			width: '100%',
			scroll: {
			  // fx : "crossfade",
			  items: 1,
			  duration: 1000,
			  easing: "easeOutExpo"
			},
			items: {
				  width: '350',
			  height: 'variable', //  optionally resize item-height
			  visible: {
				min: 1,
				max: 4
			  }
			},
			mousewheel: false,
			swipe: {
			  onMouse: true,
			  onTouch: true
			  }
		  });

		  $(window).bind("resize",updateSizes_vat).bind("load",updateSizes_vat);
		  function updateSizes_vat(){
			$('#team .carousel.main ul').trigger("updateSizes");

		  }
		  updateSizes_vat();
		}); //
		$(window).load(function() {
		  //

		  /////// flexslider
		  $('#flexslider').flexslider({
			animation: "fade",
			slideshow: true,
			slideshowSpeed: 7000,
			animationDuration: 600,
			pauseOnAction: true,
			prevText: "",
			nextText: "",
			controlNav: true,
			directionNav: true
		  });



		}); //
		
		$(document).ready(function() {
  //

  // touchTouch
  $('.thumb-isotope .thumbnail a').touchTouch();



}); //
$(window).load(function() {
  //

  /*----------------------------------------------------*/
  // ISOTOPE BEGIN
  /*----------------------------------------------------*/
  var $container = $('#container');
  //Run to initialise column sizes
  updateSize();

  //Load fitRows when images all loaded
  $container.imagesLoaded( function(){

      $container.isotope({
          // options
          itemSelector : '.element',
          layoutMode : 'fitRows',
          transformsEnabled: true,
          columnWidth: function( containerWidth ) {
              containerWidth = $browserWidth;
              return Math.floor(containerWidth / $cols);
            }
      });
  });

  // update columnWidth on window resize
  $(window).smartresize(function(){
      updateSize();
      $container.isotope( 'reLayout' );
  });

  //Set item size
  function updateSize() {
      $browserWidth = $container.width();
      $cols = 4;

      if ($browserWidth >= 1170) {
          $cols = 4;
      }
      else if ($browserWidth >= 767 && $browserWidth < 1170) {
          $cols = 3;
      }
      else if ($browserWidth >= 480 && $browserWidth < 767) {
          $cols = 2;
      }
      else if ($browserWidth >= 0 && $browserWidth < 480) {
          $cols = 1;
      }
      //console.log("Browser width is:" + $browserWidth);
      //console.log("Cols is:" + $cols);

      // $gutterTotal = $cols * 20;
      $browserWidth = $browserWidth; // - $gutterTotal;
      $itemWidth = $browserWidth / $cols;
      $itemWidth = Math.floor($itemWidth);

      $(".element").each(function(index){
          $(this).css({"width":$itemWidth+"px"});
      });



    var $optionSets = $('#options .option-set'),
        $optionLinks = $optionSets.find('a');

    $optionLinks.click(function(){
      var $this = $(this);
      // don't proceed if already selected
      if ( $this.hasClass('selected') ) {
        return false;
      }
      var $optionSet = $this.parents('.option-set');
      $optionSet.find('.selected').removeClass('selected');
      $this.addClass('selected');

      // make option object dynamically, i.e. { filter: '.my-filter-class' }
      var options = {},
          key = $optionSet.attr('data-option-key'),
          value = $this.attr('data-option-value');
      // parse 'false' as false boolean
      value = value === 'false' ? false : value;
      options[ key ] = value;
      if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
        // changes in layout modes need extra logic
        changeLayoutMode( $this, options )
      } else {
        // otherwise, apply new options
        $container.isotope( options );
      }

      return false;
    });

  };
  /*----------------------------------------------------*/
  // ISOTOPE END
  /*----------------------------------------------------*/



}); //
		
		</script>

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->		
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" /> 	
	</head>
	<body class="<?if($curPage == SITE_DIR."index.php"){?>front<?}else{?>not-front<?}?>">
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
<div id="main">
<div class="top_wrapper">
  <div class="top_bg">
    <?if($curPage == SITE_DIR."index.php"){?>
	<img src="<?=SITE_TEMPLATE_PATH?>/images/sky1.jpg" alt="" class="sky1">
	<?}else{?>
	<img src="<?=SITE_TEMPLATE_PATH?>/images/sky2.jpg" alt="" class="sky2">
	<?}?>
    <div class="grass1"></div>
    <div class="sun1"></div>
    <div class="green1"></div>
  </div>
  <div class="top_content">
    <div class="container">
      <header>
        <div class="logo_wrapper">
          <a href="<?=SITE_DIR?>" class="logo">
            <img src="<?=SITE_TEMPLATE_PATH?>/images/logo.png" alt="" class="img-responsive">
          </a>
        </div>
      </header> 
	  
<?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"top_horizontal", 
	array(
		"ROOT_MENU_TYPE" => "top",
		"MENU_CACHE_TYPE" => "A",
		"MENU_CACHE_TIME" => "36000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_THEME" => "site",
		"CACHE_SELECTED_ITEMS" => "N",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MAX_LEVEL" => "2",
		"CHILD_MENU_TYPE" => "left",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "N",
		"COMPONENT_TEMPLATE" => "top_horizontal"
	),
	false
);?>

<?if($curPage == SITE_DIR."index.php"){?>
<?$APPLICATION->IncludeComponent(
	"bit-ecommerce:slide",
	"",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_ID" => "1",
		"IBLOCK_ITEMS_COUNT" => "5"
	)
);?>  
	

      <div class="snail1_wrapper">
        <div class="tree1 animated" data-animation="fadeIn" data-animation-delay="200"><img src="<?=SITE_TEMPLATE_PATH?>/images/tree1.png" alt="" class="img-responsive"></div>
        <div class="snail1 animated" data-animation="bounceInLeft" data-animation-delay="300"><img src="<?=SITE_TEMPLATE_PATH?>/images/snail1.png" alt="" class="img-responsive"></div>
      </div>
	<div class="slogan1 animated" data-animation="fadeIn" data-animation-delay="400"><?=$APPLICATION->ShowTitle(false);?></div>  

 <div class="slogan2 animated" data-animation="fadeIn" data-animation-delay="500">
 <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/index_slogan.php"), false);?>
	</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"news_index", 
	array(
		"COMPONENT_TEMPLATE" => "news_index",
		"IBLOCK_TYPE" => "news_electronics",
		"IBLOCK_ID" => "2",
		"NEWS_COUNT" => "6",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"CACHE_TYPE" => "N",
		"CACHE_TIME" => "36000000",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "N",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "f j, Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"PAGER_TEMPLATE" => ".default",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "Y",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"TEMPLATE_THEME" => "site",
		"MEDIA_PROPERTY" => "",
		"SLIDER_PROPERTY" => "",
		"SEARCH_PAGE" => "/search/",
		"USE_RATING" => "N", 
		"USE_SHARE" => "N"
	),
	false
);?>
	<?}else{?>
     <div class="snail1_wrapper">
        <div class="tree1 animated" data-animation="fadeIn" data-animation-delay="200"><img src="<?=SITE_TEMPLATE_PATH?>/images/tree1.png" alt="" class="img-responsive"></div>
        <div class="snail1 animated" data-animation="bounceInLeft" data-animation-delay="300"><img src="<?=SITE_TEMPLATE_PATH?>/images/snail1.png" alt="" class="img-responsive"></div>
      </div>
<div class="page_title"><?=$APPLICATION->ShowTitle(false);?></div>	 

	<?}?>

	</div>
  </div>	
   <div class="wave1"></div>  
</div>	
<?if($curPage == SITE_DIR."index.php"){?><div class="splash_wrapper"><?}else{?>
<div id="content">
  
  <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"marcet", 
	array(
		"COMPONENT_TEMPLATE" => "marcet",
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "-"
	),
	false
);?>
  
    <div class="container">
    <div class="row">
      <div class="col-sm-12">
	  <h2><?=$APPLICATION->ShowTitle(false);?></h2>
<?}?>					