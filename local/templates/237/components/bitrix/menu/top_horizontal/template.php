<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$curPage = $APPLICATION->GetCurPage(true);
$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;





$menuBlockId = "catalog_menu_".$this->randString();
?>
      <div class="menu_wrapper">
        <div class="navbar navbar_ navbar-default">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <div class="navbar-collapse navbar-collapse_ collapse">
            <ul class="nav navbar-nav sf-menu clearfix">
		<?
		$n=1;
		$count = count($arResult["MENU_STRUCTURE"]);
		foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):
		
		?><?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>			
			<li	class="nav<?=$n?> <?if (is_array($arColumns) && count($arColumns) > 0){?>sub-menu sub-menu-1<?}?> <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>active<?endif?> <?=$arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']?>"><a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"	><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>							
					<?foreach($arColumns as $key=>$arRow):?>
						<ul><?foreach($arRow as $itemIdLevel_2=>$arLevel_3):?>  						
							<li><a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?>											
								</a>							
							<?if (is_array($arLevel_3) && count($arLevel_3) > 0):?>								
								<?
								$i = 0;
								foreach($arLevel_3 as $itemIdLevel_3):
								if($i<5){
								?>	<!-- third level-->
									
										<a style="display: inline-block; margin-right: 8px;"
											href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["LINK"]?>"
											<?if ($existPictureDescColomn):?>
												ontouchstart="//document.location.href = '<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>';return false;"
												onmouseover="obj_<?=$menuBlockId?>.changeSectionPicure(this, '<?=$itemIdLevel_3?>');return false;"
											<?endif?>
											data-picture="<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["PARAMS"]["picture_src"]?>">
											<?=$arResult["ALL_ITEMS"][$itemIdLevel_3]["TEXT"]?>
										</a>
									
								<?
								};
								++$i;
								endforeach;?>
								
							<?endif?>
							<?if ($arResult["ALL_ITEMS"][$itemID]['PARAMS']['CLASS']=='catalog'){?></div><?}?>
							</li>							
						<?endforeach;?>
						</ul>
					<?endforeach;?>
					</li>
			<?endif?>
					
			<?$n++;?>
		<?endforeach;?>
            </ul>
          </div>
        </div>
      </div>

