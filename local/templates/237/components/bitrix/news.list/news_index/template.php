<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
if(count($arResult["ITEMS"])>0){?>
      <div id="team_wrapper">
        <div id="team_inner">
          <div class="">
            <div id="team">
              <a class="team_prev" href="#"></a>
              <a class="team_next" href="#"></a>
              <div class="carousel-box">
                <div class="inner">
                  <div class="carousel main">
                    <ul>
<?
foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

                      <li>
                        <div class="team">
                          <div class="team_inner">
                            <a href="<?echo $arItem["CODE"]?>">
                              <figure><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="" class="img-responsive"><em></em></figure>
                              <div class="caption">
                                <div class="txt1"><?echo $arItem["NAME"]?></div>
                                <div class="txt2"><?echo $arItem["PREVIEW_TEXT_IMPLODE"];?></div>
                                <div class="txt3"><? echo GetMessage('CT_BNL_GOTO_DETAIL'); ?></div>
                              </div>
                            </a>
                          </div>
                        </div>
                      </li>
<?endforeach;?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
<?}?>

