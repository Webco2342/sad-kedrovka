<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

    <div id="options" class="clearfix">
      <ul id="filters" class="pagination option-set clearfix" data-option-key="filter">
        <li><a href="#filter" data-option-value="*" class="selected">Все</a></li>
      </ul>
    </div>

    <div class="isotope-box">
                  <div id="container" class="clearfix">
                    <ul class="thumbnails" id="isotope-items">	
<?
foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

	  
                      <li class="element isotope-filter1">
                        <div class="thumb-isotope">
                          <div class="thumbnail clearfix">
                            <a href="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>">
                              <figure>
                                <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""><em></em>
                              </figure>
                              <div class="caption"><?echo $arItem["NAME"]?></div>
                            </a>
                          </div>
                        </div>
                      </li>					  
<?endforeach;?>
				</ul>
			</div>
        </div>


