<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<div class="row">
<div class="col-md-12">

	<?

	$Sections = array();
	foreach($arResult["SECTIONS"] as $arSection)
	{?>
		 <h2 class="c"><?=$arSection['NAME']?></h2>
		 <div class="row">
		 
		 <?
$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "CODE", "PREVIEW_TEXT");
$arFilter = Array("IBLOCK_ID"=>3, "SECTION_ID"=>$arSection['ID'], "ACTIVE"=>"Y");
$res = CIBlockElement::GetList(Array("SODT"=>"ASC"), $arFilter, false, Array("nPageSize"=>500), $arSelect);
while($ob = $res->GetNextElement())
{
 $arFields = $ob->GetFields();
?>
      <div class="col-sm-12">
        <div class="thumb3">
          <div class="thumbnail clearfix">
            <a href="#">
			<div class="col-sm-3">
              <figure class="">
                <img src="<?=CFile::GetPath ($arFields["PREVIEW_PICTURE"])?>" alt="" class="img-responsive">
              </figure>
			  </div>
              <div class="caption col-sm-6">
                <div class="txt1"><?=$arFields['NAME']?></div>
                <div class="txt2"><strong><?=$arFields['CODE']?></strong></div>
				<div class="txt2"><?=$arFields['PREVIEW_TEXT']?></div>
              </div>
            </a>
          </div>
        </div>
      </div>
<?
}
?> 
		 
		 </div>
	<?}?>
 </div>	
 </div>
