<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454244452';
$dateexpire = '001490244452';
$ser_content = 'a:2:{s:7:"CONTENT";s:8957:"<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_22">

						<div class="bx-newsdetail-img">
				<img
					src="/upload/iblock/10f/10f15f779e5105db7e6a6a38133a5f90.jpg"
					width="600"
					height="330"
					alt="Адаптация"
					title="Адаптация"
					/>
			</div>
			
	
	<div class="bx-newsdetail-content">
			<p align="CENTER">
	 Как надо готовить родителям ребенка к поступлению в детский сад.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<ul>
	<li>
	<p>
		 Отправить в детский сад ребенка лишь при условии, что он здоров. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не отдавать ребенка в детский сад в разгаре кризиса трех лет. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Заранее узнать все новые моменты в режиме дня в детском саду и ввести их в&nbsp; режим дня для ребенка.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Повысит роль закаливающих мероприятий &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Как можно раньше познакомить малыша с детьми в детском саду и воспитателям&nbsp; группы, куда он в скором времени придет. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Настроить малыша как можно положительнее к его поступлению в детсад. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Раскрыть малышу «секреты» возможности навыков общения с детьми и&nbsp; взрослыми. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Учить ребенка дома всем необходимым навыкам самообслуживания. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Готовить Вашего ребенка к временной разлуке с Вами и дать понять ему, что это &nbsp;неизбежно только потому, что он уже большой. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не нервничать и не показывать свою тревогу накануне поступления ребенка в детский сад. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Планировать свой отпуск так, чтобы в первый месяц посещения ребенком нового организационного коллектива у Вас была возможность оставлять его там не на целый&nbsp; день. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Все время объяснять ребенку, что он для Вас, как и прежде, дорог и любим &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Внушать ему, что это очень здорово, что&nbsp;&nbsp; он дорос до сада, и стал таким большим.
	</p>
 </li>
</ul>
<p>
 <br>
 <br>
 <br>
</p>
<p align="CENTER">
	 Как надо вести себя родителям с ребенком, когда он впервые начал посещать детский&nbsp; сад.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<ul>
	<li>
	<p>
		 Не оставлять его в дошкольном коллективе на целый&nbsp; день, как можно раньше забирать домой. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Создать спокойный бесконфликтный климат для него&nbsp; семье. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Щадить его ослабленную нервную систему. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не&nbsp; увеличивать, а уменьшать нагрузку на нервную&nbsp; систему. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 На время прекратить походы в цирк, в театр, в гости. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Как можно раньше сообщить врачу и воспитателям о личностных особенностях малыша. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не кутать ребенка, а одевать его так, как необходимо&nbsp; в соответствии с температурой в группе. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Создать в выходные дни дома для него режим такой же, как и в детском учреждении. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не реагировать на выходки ребенка и не наказывать его &nbsp;за капризы. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 При выявленном изменении в обычном поведении ребенка как можно раньше обратиться к детскому врачу. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 При выражении невротических реакций оставить малыша на несколько дней дома, и выполнять все&nbsp; предписания врача.
	</p>
 </li>
</ul>
<p>
 <br>
 <br>
 <br>
</p>
<p align="CENTER">
	 Как не надо вести себя родителям с ребенком, когда он впервые начал посещать детский сад. &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<ul>
	<li>
	<p>
		 В присутствии ребенка плохо говорить о детском саде. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 «Наказывать» ребенка детским садом и поздно забирать домой. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Мешать его контактам с детьми в группе. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не уменьшать, а увеличивать нагрузку на нервную систему. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Водить ребенка в многолюдные и шумные места. &nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Все время кутать, одевать не по сезону. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Конфликтовать с ним дома. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Наказывать за капризы. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 В выходные дни резко изменять режим дня ребенка. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Все время обсуждать в его присутствии проблемы, связанные с детским садом. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не обращать внимания на отклонения в обычном поведении ребенка. &nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не выполнять все предписания врача, психолога. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	</p>
 </li>
	<li>
	<p>
		 Не обсуждать при малыше волнующие Вас проблемы, связанные с детским садом.
	</p>
 </li>
</ul>
<div>
 <br>
	 Фото с сайта:&nbsp;cityforkids.ru<br>
</div>
 <br>		</div>

		
			<div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i> 31.01.2016</div>
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_22\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:2:"22";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:18:"Адаптация";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:12:"~TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:15:"~IBLOCK_TYPE_ID";s:9:"podsolnuh";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:0:"";s:5:"~CODE";s:0:"";s:4:"NAME";s:32:"Для Вас, родители!";s:5:"~NAME";s:32:"Для Вас, родители!";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:17:"~SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:31:"подсолнушек-237.рф";s:12:"~SERVER_NAME";s:31:"подсолнушек-237.рф";}s:13:"LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:14:"~LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"31.01.2016 07:24:35";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/style.css";s:12:"additionalJS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:74:"/bitrix/components/bitrix/news.detail/templates/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>