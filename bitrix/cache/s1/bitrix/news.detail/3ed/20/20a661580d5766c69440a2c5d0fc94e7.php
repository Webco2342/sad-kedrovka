<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454368690';
$dateexpire = '001490368690';
$ser_content = 'a:2:{s:7:"CONTENT";s:6608:"<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_25">

						<div class="bx-newsdetail-img">
				<img
					src="/upload/iblock/160/1604e7eb22c0569d0bcc359997760791.jpg"
					width="3648"
					height="2736"
					alt="Компенсация родительской платы"
					title="Компенсация родительской платы"
					/>
			</div>
			
	
	<div class="bx-newsdetail-content">
			<p align="CENTER">
	 ИНФОРМАЦИЯ
</p>
<p align="CENTER">
	 о назначении и выплате ежемесячной компенсации части родительской платы за содержание детей в дошкольном образовательном учреждении
</p>
<p align="JUSTIFY">
</p>
<p>
	 &nbsp;&nbsp;&nbsp;&nbsp; Право на получение компенсации в соответствии с Постановлением Коллегии Кемеровской области&nbsp; от 13.02.2007 № 34 «Об утверждении Порядка обращения и выплаты компенсации части родительской платы за содержание ребенка в образовательных учреждениях, реализующих основную общеобразовательную программу дошкольного образования»&nbsp; имеет гражданин, внесший родительскую плату за содержание ребенка в образовательных учреждениях.
</p>
<p>
</p>
<p>
	 &nbsp;&nbsp;&nbsp;&nbsp; Компенсация&nbsp;устанавливается в следующих размерах:
</p>
<p>
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 20 процентов&nbsp;от уплаченной суммы родительской платы за содержание ребенка в учреждении –&nbsp;на первого&nbsp;по очередности рождения ребенка;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 50 процентов&nbsp;от уплаченной суммы родительской платы за содержание ребенка в учреждении –&nbsp;на второго&nbsp;по очередности рождения ребенка;
</p>
<p>
	 -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 70 процентов&nbsp;от уплаченной суммы родительской платы за содержание ребенка в учреждении –&nbsp;на третьего&nbsp;по очередности рождения ребенка.
</p>
<p>
</p>
<p>
	 &nbsp;&nbsp;&nbsp; При установлении очередности рождения детей в семье исключаются дети, достигшие возраста 18 лет, в случае одновременного рождения двух и более детей один ребенок считается первым, другой вторым и т. д. Размер компенсации пересматривается в случае достижения старшим ребенком возраста 18 лет.
</p>
<p align="JUSTIFY">
</p>
<p align="JUSTIFY">
	 &nbsp;&nbsp;&nbsp;&nbsp; Компенсация назначается на основании заявления гражданина и следующих документов (копий документов):
</p>
<p align="JUSTIFY">
	  паспорта гражданина;
</p>
<p align="JUSTIFY">
	  свидетельства о рождении ребенка;
</p>
<p align="JUSTIFY">
	  справки о составе семьи;
</p>
<p align="JUSTIFY">
	  справки, подтверждающей посещение ребенком дошкольного образовательного учреждения;
</p>
<p align="JUSTIFY">
	  сберегательной книжки;
</p>
<p align="JUSTIFY">
	 в случае необходимости:
</p>
<p align="JUSTIFY">
	  выписки из решения органа местного самоуправления об установлении над ребенком опеки (для опекуна);
</p>
<p align="JUSTIFY">
	  выписки из решения органа местного самоуправления о передаче ребенка на воспитание в приемную семью (для приемного родителя);
</p>
<p align="JUSTIFY">
	  свидетельств о заключении брака, о перемене имени (копия и подлинник) при смене фамилии, имени родителем и ребенком.&nbsp;&nbsp;
</p>
<p align="JUSTIFY">
	 &nbsp;&nbsp;&nbsp;&nbsp;
</p>
<p align="CENTER">
	 За назначением компенсации граждане обращаются с документами в ДОУ, который посещает ребенок. Компенсации назначаются и выплачиваются&nbsp;&nbsp;по месту нахождения ДОУ&nbsp;&nbsp;за истекший период, начиная с 01.01.2007 г, но не более чем за 6 месяцев с момента обращения за назначением компенсации.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</p>
<p align="CENTER">
</p>
<p align="CENTER">
	 Выплата компенсации производится ежемесячно.
</p>
<p align="CENTER" style="text-align: left;">
	 Фото с сайта:&nbsp;vbratske.ru
</p>		</div>

		
			<div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i> 30.01.2016</div>
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_25\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:2:"25";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:58:"Компенсация родительской платы";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:12:"~TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:15:"~IBLOCK_TYPE_ID";s:9:"podsolnuh";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:0:"";s:5:"~CODE";s:0:"";s:4:"NAME";s:32:"Для Вас, родители!";s:5:"~NAME";s:32:"Для Вас, родители!";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:17:"~SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:31:"подсолнушек-237.рф";s:12:"~SERVER_NAME";s:31:"подсолнушек-237.рф";}s:13:"LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:14:"~LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"31.01.2016 07:30:17";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/style.css";s:12:"additionalJS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:74:"/bitrix/components/bitrix/news.detail/templates/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>