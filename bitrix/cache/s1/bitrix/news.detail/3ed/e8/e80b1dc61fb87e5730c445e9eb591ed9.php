<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454368700';
$dateexpire = '001490368700';
$ser_content = 'a:2:{s:7:"CONTENT";s:7420:"<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_26">

						<div class="bx-newsdetail-img">
				<img
					src="/upload/iblock/526/526e7e4882658654e19d2c6cc62e4940.jpg"
					width="1024"
					height="768"
					alt="Материнский капитал"
					title="Материнский капитал"
					/>
			</div>
			
	
	<div class="bx-newsdetail-content">
			<p>
	 АДМИНИСТРАЦИЯ ГОРОДА
</p>
<p>
	 КЕМЕРОВО
</p>
<p>
	 УПРАВЛЕНИЕ ОБРАЗОВАНИЯ&nbsp;Руководителям
</p>
<p>
	 просп. Советский, 54, г. Кемерово, 650000&nbsp;дошкольных образовательных
</p>
<p>
	 тел. 36-46-19, факс 36-46-19 учреждений города Кемерово,
</p>
<p>
	 e-mail:<a href="mailto:edu@kemerovo.ru">edu@kemerovo.ru</a>&nbsp;Начальнику МБУ «Централизованная бухгалтерия управления образования»
</p>
<p>
	 ____________________ № __________________ Л.А. Альберт
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
	 Уважаемые коллеги!
</p>
<p>
</p>
<p>
	 Доводим до Вашего сведения, что в соответствии с постановлением Правительства Российской Федерации от 24.12.2011 г. № 926 (в редакции постановления Правительства РФ от 14.11.2011 г. № 931) (далее – Правила), предусмотрена возможность направления средств на оплату содержания ребенка в образовательном учреждении, реализующем основную общеобразовательную программу дошкольного образования.
</p>
<p>
	 В соответствии с 8(2) и 8 (3) Правил к заявлению о распоряжении средствами прилагается договор между образовательным учреждением и родителем (законным представителем), получившим государственный сертификат на материнский (семейный) капитал (далее – сертификат), включающий в себя обязательства учреждения по содержанию ребенка в образовательном учреждении и расчет размера платы за содержание ребенка в образовательном учреждении (далее – договор).
</p>
<p>
	 Средства направляются на оплату содержания ребенка в образовательном учреждении путем безналичного перечисления этих средств на счета (лицевые счета) образовательного учреждения, указанные в договоре.
</p>
<p>
	 Таким образом, если лицо, получившее сертификат, принимает решение направить средства на оплату содержания ребенка в образовательном учреждении, то в договоре с ним рекомендуется дополнительно указывать:
</p>
<p>
	 - реквизиты для перечисления средств (наименование образовательного учреждения, ИНН, БИК, КПП, банк получателя, ОКАТО, КБК);
</p>
<p>
	 - размер платы за содержание ребенка в образовательном учреждении, согласно решению Кемеровского городского Совета народных депутатов № 308 от 27.11.2009 г. включающий в себя сумму средств, подлежащих возврату в качестве компенсации части родительской платы за содержание ребенка в образовательном учреждении, с указанием суммы средств для направления органами Пенсионного фонда РФ на оплату содержания ребенка в образовательном учреждении;
</p>
<p>
	 - срок (сроки) направления средств (ежемесячно, ежеквартально, одной сумой за учебный год);
</p>
<p>
	 - возможность учета при последующих платежах сумм, образовавшихся в конце учебного года в результате превышения перечисленных по договору сумм над фактическими расходами за содержание ребенка в образовательном учреждении (при условии использования образовательным учреждением расчета размера платы, взимаемой с родителей (законных представителей) за содержание ребенка, учитывающего его фактическое пребывание в образовательном учреждении;
</p>
<p>
	 - возможность возврата образовательным учреждением в территориальные органы Пенсионного фонда РФ неиспользованных средств в случае расторжения договора по причинам, указанным в пункте 12 Правил, или истечения срока действия договора.
</p>
<p>
	 Информация направляется для сведения и использования руководителями образовательных учреждений при заключении договора с родителями (законными представителями) или внесении в указанные договора дополнений и изменений.
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
	 Начальник управления образования Н.А. Чернова
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
	 исп. Крючкова М.С.,
</p>
<p>
	 тел. 58-34-27
</p>
<p>
 <br>
</p>
<p>
	 Фото с сайта:&nbsp;piaspb.ru
</p>		</div>

		
			<div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i> 30.01.2016</div>
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_26\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:2:"26";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:37:"Материнский капитал";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:12:"~TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:15:"~IBLOCK_TYPE_ID";s:9:"podsolnuh";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:0:"";s:5:"~CODE";s:0:"";s:4:"NAME";s:32:"Для Вас, родители!";s:5:"~NAME";s:32:"Для Вас, родители!";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:17:"~SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:31:"подсолнушек-237.рф";s:12:"~SERVER_NAME";s:31:"подсолнушек-237.рф";}s:13:"LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:14:"~LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"31.01.2016 07:34:23";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/style.css";s:12:"additionalJS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:74:"/bitrix/components/bitrix/news.detail/templates/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>