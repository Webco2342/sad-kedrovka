<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454368712';
$dateexpire = '001490368712';
$ser_content = 'a:2:{s:7:"CONTENT";s:5762:"<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_29">

						<div class="bx-newsdetail-img">
				<img
					src="/upload/iblock/c62/c6275c882527ab13b57af9aa1c056c78.jpg"
					width="1920"
					height="1080"
					alt="Правила для родителей"
					title="Правила для родителей"
					/>
			</div>
			
	
	<div class="bx-newsdetail-content">
			<p align="CENTER">
	 Правила для родителей
</p>
<p>
	 &nbsp;•&nbsp;Прием детей осуществляется с 7.00 до 8.00 ежедневно, кроме выходных и праздничных дней. Своевременный приход в детский сад - необходимое условие правильной организации воспитательно-образовательного процесса.<br>
	 &nbsp;• В отпуск воспитанник уходит &nbsp;на 75 дней, с сохранением места в детском саду.&nbsp;<br>
	 • Все дети возвращаются с летних каникул &nbsp;со справкой об отсутствии контактов с инфекционными больными и о состоянии здоровья из поликлиники.<br>
	 &nbsp;• Педагоги готовы пообщаться с Вами утром до 8.15 и вечером после 17.00. В другое время педагог работает с группой детей, и отвлекать его не рекомендуется.<br>
	 &nbsp;• К педагогам группы, независимо от их возраста, необходимо обращаться на Вы, по имени и отчеству. Конфликтные спорные ситуации необходимо разрешать в отсутствие детей. Если вы не смогли решить какой-либо вопрос с педагогами группы, обратитесь к старшему воспитателю или заведующему.<br>
	 &nbsp;• В детском саду работает служба психологической помощи, куда вы сможете обратиться за консультацией по интересующим Вас вопросам.&nbsp;<br>
	 • Просим Вас не давать ребенку с собой в детский сад жевательную резинку, сосательные конфеты, чипсы и сухарики.<br>
	 &nbsp;• Настоятельно не рекомендуем одевать ребенку золотые и серебряные украшения, давать с собой дорогостоящие игрушки.&nbsp;
</p>
<p>
</p>
<p align="CENTER">
	 Требования к внешнему виду детей&nbsp;<br>
 <br>
 <br>
</p>
<p>
	 •&nbsp;Опрятный вид, застегнутая на все пуговицы одежда и обувь;&nbsp;<br>
	 • Умытое лицо; • Чистые нос, руки, подстриженные ногти;<br>
	 &nbsp;• Подстриженные и тщательно расчесанные волосы;&nbsp;<br>
	 • Чистое нижнее белье;&nbsp;<br>
	 • Наличие достаточного количества носовых платков. Перед тем, как вести ребенка в детский сад, проверьте, соответствует ли его костюм времени года и температуре воздуха. Проследите, чтобы одежда не была слишком велика и не сковывала его движений. Завязки и застежки должны быть расположены так, чтобы ребенок мог самостоятельно себя обслужить. Обувь должна быть легкой, теплой, точно соответствовать ноге ребенка, легко сниматься и надеваться. Нежелательно ношение комбинезонов. Носовой платок необходим ребенку как в помещении, так и на прогулке. Сделайте на одежде удобные карманы для его хранения. Чтобы избежать случаев травматизма, родителям необходимо проверить содержимое карманов в одежде ребенка на наличие опасных предметов. Категорически запрещается приносить в ДОУ острые, режущие стеклянные предметы (ножницы, ножи, булавки, гвозди, проволоку, зеркала, стеклянные флаконы), а также мелкие предметы (бусинки, пуговицы и т.п.), таблетки.
</p>
 Фото с сайта:&nbsp;nanocenter.tsutmb.ru<br>
 <br>		</div>

		
			<div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i> 30.01.2016</div>
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_29\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:2:"29";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:40:"Правила для родителей";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:12:"~TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:15:"~IBLOCK_TYPE_ID";s:9:"podsolnuh";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:0:"";s:5:"~CODE";s:0:"";s:4:"NAME";s:32:"Для Вас, родители!";s:5:"~NAME";s:32:"Для Вас, родители!";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:17:"~SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:31:"подсолнушек-237.рф";s:12:"~SERVER_NAME";s:31:"подсолнушек-237.рф";}s:13:"LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:14:"~LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"31.01.2016 07:46:29";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/style.css";s:12:"additionalJS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:74:"/bitrix/components/bitrix/news.detail/templates/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>