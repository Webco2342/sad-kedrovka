<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454368715';
$dateexpire = '001490368715';
$ser_content = 'a:2:{s:7:"CONTENT";s:15700:"<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_30">

						<div class="bx-newsdetail-img">
				<img
					src="/upload/iblock/bee/beea8c55f488466f289e0cc5d5e4111c.jpg"
					width="1200"
					height="829"
					alt="Расчет компенсации родительской платы"
					title="Расчет компенсации родительской платы"
					/>
			</div>
			
	
	<div class="bx-newsdetail-content">
			<p>
	 Уважаемые родители!
</p>
<p>
 <br>
</p>
<p align="JUSTIFY">
	 В связи с внесением изменений в&nbsp;постановление Коллегии Администрации Кемеровской области от 30.09.2013 № 410 «О компенсации платы, взимаемой с родителей (законных представителей) за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в образовательных организациях, осуществляющих образовательную деятельность и находящихся на территории Кемеровской области»&nbsp;изменился расчет компенсации&nbsp;платы, взимаемой с родителей (законных представителей) за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в образовательных организациях, осуществляющих образовательную деятельность (далее – компенсация).
</p>
<p>
	 Начиная с&nbsp;01.07.2014 года,&nbsp;выплата&nbsp;компенсации будет производиться исходя из среднего размера родительской платы, установленной Коллегией АКО, в размере 1100 рублей и коэффициента фактической посещаемости ребенка.
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 1.Размер компенсации на первого ребенка рассчитывается по формуле:
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="CENTER">
	 Разм.ком.К1 = Р род. пл. х 0,2 х Ффакт
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 Разм.ком.К1 – размер компенсации;
</p>
<p align="JUSTIFY">
	 Р род. пл. -&nbsp;средний размер платы, взимаемой с родителей (законных представителей) за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в государственных и муниципальных образовательных организациях, устанавливаемый Коллегией Администрации Кемеровской области (с 01.07.2014 составляет 1100 рублей);
</p>
<p align="JUSTIFY">
	 0,2 - 20 процентов от среднего размера родительской платы, установленного Коллегией Администрации Кемеровской области, но не более фактически внесенной родительской платы – на первого по очередности рождения ребенка;
</p>
<p align="JUSTIFY">
	 Ффакт - фактическая посещаемость ребенка образовательной организации, реализующей образовательную программу дошкольного образования, в год (месяц).
</p>
<p align="JUSTIFY">
	 Например, Иванова Елизавета - первый ребенок в семье и посещает детский сад. Размер установленной родительской платы за&nbsp;присмотр и уход за детьми&nbsp;в данной образовательной организации, реализующей образовательную программу дошкольного образования, составляет 1900 рублей в месяц. Мама Елизаветы внесла родительскую плату за&nbsp;присмотр и уход&nbsp;в ДОУ&nbsp;за июль 2014г.&nbsp;в размере 1700 рублей, так как Елизавета посещала детский сад неполный месяц.
</p>
<p align="JUSTIFY">
	 Рассчитаем размер компенсации на Иванову Елизавету.
</p>
<p align="JUSTIFY">
	 1) Фактическая посещаемость Елизаветы за июль определяется по формуле:
</p>
<p align="JUSTIFY">
	 Ффакт = фактическое начисление/размер установленной платы план х детодни посещения план, мес.
</p>
<p align="JUSTIFY">
	 Подставим значения в уравнение
</p>
<p align="JUSTIFY">
	 Ффакт = 1700 рублей/1900 х 1 мес. = 0,895 мес.
</p>
<p align="JUSTIFY">
	 2) Размер компенсации Елизаветы определяется по формуле пункта 2.1 настоящих рекомендаций:
</p>
<p align="CENTER">
	 Разм.ком.К1 = Р род. пл. х 0,2 х Ффакт
</p>
<p align="JUSTIFY">
	 Подставим значения в уравнение
</p>
<p align="CENTER">
	 Разм.ком. К1 = 1100 руб х 0,2 х 1 х 0,895 мес. = 197 рублей
</p>
<p align="JUSTIFY">
	 3) Размер компенсации маме Елизаветы за июль 2014 года составит&nbsp;197&nbsp;рублей.
</p>
<p align="JUSTIFY">
	 2. Размер компенсации на второго ребенка рассчитывается по формуле:
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="CENTER">
	 Разм.ком.К2 = Р род. пл. х 0,5 х Ффакт
</p>
<p align="JUSTIFY">
	 Разм.ком.К2 – размер компенсации;
</p>
<p align="JUSTIFY">
	 Р род. пл. -&nbsp;средний размер платы, взимаемой с родителей (законных представителей) за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в государственных и муниципальных образовательных организациях, устанавливаемый Коллегией Администрации Кемеровской области (с 01.07.2014 составляет 1100 рублей);
</p>
<p align="JUSTIFY">
	 0,5 - 50 процентов от среднего размера родительской платы, установленного Коллегией Администрации Кемеровской области, но не более фактически внесенной родительской платы – на второго по очередности рождения ребенка;
</p>
<p align="JUSTIFY">
	 Ффакт - фактическая посещаемость ребенка образовательной организации, реализующей образовательную программу дошкольного образования, в год (месяц).
</p>
<p align="JUSTIFY">
	 Например, Петрова Анна - второй ребенок в семье и посещает детский сад. Размер установленной родительской платы за&nbsp;присмотр и уход за детьми&nbsp;в данной образовательной организации, реализующей основную общеобразовательную программу дошкольного образования, составляет 1900 рублей в месяц. Мама Анны внесла родительскую плату за&nbsp;присмотр и уход&nbsp;в ДОУ&nbsp;за июль 2014г.&nbsp;в размере 1700 рублей, так как Анна посещала детский сад неполный месяц.
</p>
<p align="JUSTIFY">
	 Рассчитаем размер компенсации части родительской платы на Петрову Анну.
</p>
<p align="JUSTIFY">
	 1) Фактическая посещаемость Анны за июль определяется по формуле:
</p>
<p align="JUSTIFY">
	 Ффакт = фактическое начисление/размер установленной род.платы план х детодни посещения план, мес.
</p>
<p align="JUSTIFY">
	 Подставим значения в уравнение
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 Ффакт = 1700 рублей/1900 х 1 мес. = 0,895 мес.
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 2) Размер компенсации части родительской платы за&nbsp;присмотр и уход&nbsp;Анны определяется по формуле пункта 2.2 настоящих рекомендаций:
</p>
<p align="CENTER">
	 Разм.ком.К2 = Р род. пл. ср. х 0,5 х Ффакт
</p>
<p align="JUSTIFY">
	 Подставим значения в уравнение
</p>
<p align="CENTER">
	 Разм.ком.К2 = 1100 руб х 0,5 х 1 х 0,895 мес. = 492,25 рублей
</p>
<p align="JUSTIFY">
	 3) Размер компенсации маме Анны за июль 2014 года составит&nbsp;492,25 рублей.
</p>
<p align="JUSTIFY">
	 3. Размер компенсации на третьего ребенка рассчитывается по формуле:
</p>
<p align="CENTER">
	 Разм.ком.К3 = Р род. пл. х 0,7 х Ффакт
</p>
<p align="JUSTIFY">
	 Разм.ком.К3 – размер компенсации;
</p>
<p align="JUSTIFY">
	 Р род. пл. -&nbsp;средний размер платы, взимаемой с родителей (законных представителей) за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в государственных и муниципальных образовательных организациях, устанавливаемый Коллегией Администрации Кемеровской области (с 01.07.2014 составляет 1100 рублей);
</p>
<p align="JUSTIFY">
	 0,7 - 70 процентов от среднего размера родительской платы, установленного Коллегией Администрации Кемеровской области, но не более фактически внесенной родительской платы – на третьего и последующих по очередности рождения детей;
</p>
<p align="JUSTIFY">
	 Ффакт - фактическая посещаемость ребенка образовательной организации, реализующей образовательную программу дошкольного образования, в год (месяц).
</p>
<p align="JUSTIFY">
	 Например, Сидорова Марина, третий ребенок в семье и посещает детский сад. Размер установленной родительской платы за&nbsp;присмотр и уход&nbsp;ребенка в данной образовательной организации, реализующей образовательную программу дошкольного образования, составляет 1900 рублей в месяц. В муниципальном образовании, в котором находится детский сад Марины, сохранена льгота по оплате за детский сад многодетным семьям (50% от размера платы). Марина посещала детский сад полный месяц и ее мама внесла родительскую плату за&nbsp;присмотр и уход&nbsp;ребенка в ДОУ&nbsp;за июль 2014г.&nbsp;950 рублей, так как размер родительской платы на нее составил 50% от начисленной суммы.
</p>
<p align="JUSTIFY">
	 Рассчитаем размер компенсации части родительской платы на Марину.
</p>
<p align="JUSTIFY">
	 1) Фактическая посещаемость Марины за июль определяется по формуле:
</p>
<p align="JUSTIFY">
	 Ффакт = фактическое начисление/размер установленной род.платы план х детодни посещения план, мес.
</p>
<p align="JUSTIFY">
	 Подставим значения в уравнение
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 Ффакт = 950 рублей*2/1900 х 1 мес. = 1 мес.
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 2) Размер компенсации части родительской платы за&nbsp;присмотр и уход&nbsp;Марины определяется по формуле пункта 2.3 настоящих рекомендаций:
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="CENTER">
	 Разм.ком.К3 = Р род. пл. х 0,7 х Ффакт
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 Подставим значения в уравнение
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="CENTER">
	 Разм.ком.К3 = 1100 руб. х 0,7 х 1 х 1 мес. = 770 рублей
</p>
<p align="JUSTIFY">
 <br>
 <br>
</p>
<p align="JUSTIFY">
	 3) Размер компенсации маме Марины за июль 2014 года составит&nbsp;770&nbsp;рублей.
</p>
<p align="JUSTIFY">
 <br>
</p>
<p align="JUSTIFY">
	 Фото с сайта: 1swissru-consultinggroup.com
</p>		</div>

		
			<div class="bx-newsdetail-date"><i class="fa fa-calendar-o"></i> 30.01.2016</div>
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_30\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:2:"30";s:9:"IBLOCK_ID";s:1:"4";s:4:"NAME";s:71:"Расчет компенсации родительской платы";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"4";s:3:"~ID";s:1:"4";s:11:"TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:12:"~TIMESTAMP_X";s:19:"23.01.2016 13:43:55";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:15:"~IBLOCK_TYPE_ID";s:9:"podsolnuh";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:0:"";s:5:"~CODE";s:0:"";s:4:"NAME";s:32:"Для Вас, родители!";s:5:"~NAME";s:32:"Для Вас, родители!";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:14:"~LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=4";s:15:"DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:17:"~SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:31:"подсолнушек-237.рф";s:12:"~SERVER_NAME";s:31:"подсолнушек-237.рф";}s:13:"LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:14:"~LIST_PAGE_URL";s:19:"/dlya-vas-roditeli/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"31.01.2016 08:01:25";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/style.css";s:12:"additionalJS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:74:"/bitrix/components/bitrix/news.detail/templates/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>