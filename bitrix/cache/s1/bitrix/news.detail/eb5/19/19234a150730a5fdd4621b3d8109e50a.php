<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454178166';
$dateexpire = '001490178166';
$ser_content = 'a:2:{s:7:"CONTENT";s:6707:"<div class="bx-newsdetail">
	<div class="bx-newsdetail-block" id="bx_1878455859_21">

						<div class="bx-newsdetail-img">
				<img
					src="/upload/iblock/424/424118111be4f8e5490c1859039804ba.jpg"
					width="660"
					height="439"
					alt="Блюда из бобовых: вкусные рецепты для детей от 1,5 лет"
					title="Блюда из бобовых: вкусные рецепты для детей от 1,5 лет"
					/>
			</div>
			
	
	<div class="bx-newsdetail-content">
			<h2>Блюда из бобовых очень полезны благодаря содержанию ценного растительного белка и клетчатки. Поэтому обязательно выберите что-нибудь из наших рецептов для своего малыша.</h2>
 <br>
 <br>
<p>
	 Многие производители детского питания выпускают еду для самых маленьких, в которой сочетают зеленый горошек, зеленую фасоль, молодые стручковые бобы с разными добавками или в чистом виде. Педиатр посоветует вам, с какого возраста можно начинать кормить кроху из этих баночек. А начиная с середины второго года жизни, &nbsp;вы можете уже самостоятельно готовить для карапуза эти овощи.
</p>
 <br>
<p>
	 Секрет вкусного блюда из бобовых – в правильной технологии их приготовления. Опускать свежий или замороженный горошек, как и спаржевую фасоль, следует только в кипящую несоленую воду. А варить их &nbsp;надо на сильном огне, не прикрывая кастрюлю крышкой. Если вода во время варки выкипела раньше времени, не стоит доливать холодную воду – только кипяток. Соль нужно добавлять лишь за несколько минут до окончания варки. Не стоит варить бобовые в кислой среде, например, вместе с помидорами или с квашеной капустой. Добавляйте эти овощи в самом конце приготовления.
</p>
<h3>Суп-крем из зеленого горошка (с 1,5 лет)</h3>
<p>
	 Состав:&nbsp;
</p>
<ul>
	<li>200 г. &nbsp;мороженого горошка&nbsp;</li>
	<li>50 г. творога</li>
	<li>2 ст. ложки сметаны</li>
	<li>соль</li>
	<li>зелень укропа&nbsp; &nbsp;</li>
</ul>
<p>
	 Приготовление:
</p>
<ol>
	<li>Вскипятите 500 мл воды, высыпьте в кастрюлю горошек, отварите до мягкости и немного остудите.&nbsp;</li>
	<li>Затем добавьте в эту же кастрюлю творог и сметану, аккуратно взбейте все миксером до кремообразного состояния.&nbsp;</li>
	<li>При подаче посыпьте суп укропом и зеленым горошком.&nbsp;</li>
</ol>
 <br>
 <br>
<h3>Овощное ассорти (с 1,5 лет)</h3>
<p>
	 Состав:&nbsp;
</p>
<ul>
	<li>кочан цветной капусты&nbsp;</li>
	<li>1 морковь</li>
	<li>по 1/2 стакана зеленого горошка и фасоли</li>
	<li>соль &nbsp;</li>
</ul>
<p>
	 Приготовление:
</p>
<ol>
	<li>Цветную капусту разберите на соцветия и отварите.&nbsp;</li>
	<li>Кубики моркови обжарьте на растительном масле.&nbsp;</li>
	<li>Сложите все овощи в сотейник, добавьте зеленый горошек и зеленую фасоль, влейте немного кипятка.&nbsp;</li>
	<li>Посолите и тушите, накрыв крышкой, 15 минут.</li>
</ol>
<h3>Суп со спаржевой фасолью &nbsp;(с 2 лет)</h3>
 <br>
 <br>
<p>
	 Состав:&nbsp;
</p>
<ul>
	<li>1 л. воды&nbsp;</li>
	<li>2 картофелины&nbsp;</li>
	<li>1 луковица&nbsp;</li>
	<li>1 морковь</li>
	<li>200 г. спаржевой фасоли</li>
	<li>сметана</li>
	<li>петрушка &nbsp;</li>
</ul>
<p>
	 Приготовление:
</p>
<ol>
	<li>В кипящую воду положите нарезанный лук и &nbsp;картофель.&nbsp;</li>
	<li>Через 15 минут добавьте разрезанную на 2–3 части спаржевую фасоль и морковь, натертую на крупной терке. Варите 15–20 минут. В самом конце положите зелень петрушки. &nbsp;&nbsp;</li>
</ol>
<h3>Зеленая фасоль с чесноком &nbsp;(с 2 лет)</h3>
<p>
	 Состав:&nbsp;
</p>
<ul>
	<li>250 г. спаржевой фасоли&nbsp;</li>
	<li>2 зубчика чеснока</li>
	<li>2 ст. ложки сметаны&nbsp;</li>
	<li>10 г. сливочного масла</li>
	<li>соль &nbsp;</li>
</ul>
<p>
	 Приготовление:
</p>
<ol>
	<li>Спаржевую фасоль &nbsp;отварите в подсоленой воде, остудите и нарежьте.</li>
	<li>Чеснок измельчите и слегка обжарьте на сливочном масле. &nbsp;</li>
	<li>Затем добавьте туда фасоль и сметану, посолите, доведите до кипения и снимите с огня.&nbsp;</li>
</ol>
 <br>
<p>
	 Источник: 2mm.ru
</p>
<p>
	 Источник фото:&nbsp;lady.tochka.net<br>
</p>		</div>

		
		
	<div class="row">
		<div class="col-xs-5">
		</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	BX.ready(function() {
		var slider = new JCNewsSlider(\'bx_1878455859_21\', {
			imagesContainerClassName: \'bx-newsdetail-slider-container\',
			leftArrowClassName: \'bx-newsdetail-slider-arrow-container-left\',
			rightArrowClassName: \'bx-newsdetail-slider-arrow-container-right\',
			controlContainerClassName: \'bx-newsdetail-slider-control\'
		});
	});
</script>
";s:4:"VARS";a:2:{s:8:"arResult";a:11:{s:2:"ID";s:2:"21";s:9:"IBLOCK_ID";s:1:"5";s:4:"NAME";s:95:"Блюда из бобовых: вкусные рецепты для детей от 1,5 лет";s:17:"IBLOCK_SECTION_ID";N;s:6:"IBLOCK";a:88:{s:2:"ID";s:1:"5";s:3:"~ID";s:1:"5";s:11:"TIMESTAMP_X";s:19:"23.01.2016 14:01:12";s:12:"~TIMESTAMP_X";s:19:"23.01.2016 14:01:12";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:15:"~IBLOCK_TYPE_ID";s:9:"podsolnuh";s:3:"LID";s:2:"s1";s:4:"~LID";s:2:"s1";s:4:"CODE";s:0:"";s:5:"~CODE";s:0:"";s:4:"NAME";s:27:"Здоровый малыш";s:5:"~NAME";s:27:"Здоровый малыш";s:6:"ACTIVE";s:1:"Y";s:7:"~ACTIVE";s:1:"Y";s:4:"SORT";s:3:"500";s:5:"~SORT";s:3:"500";s:13:"LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=5";s:14:"~LIST_PAGE_URL";s:25:"/podsolnuh/index.php?ID=5";s:15:"DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"~DETAIL_PAGE_URL";s:47:"#SITE_DIR#/podsolnuh/detail.php?ID=#ELEMENT_ID#";s:16:"SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:17:"~SECTION_PAGE_URL";s:53:"#SITE_DIR#/podsolnuh/list.php?SECTION_ID=#SECTION_ID#";s:18:"CANONICAL_PAGE_URL";s:0:"";s:19:"~CANONICAL_PAGE_URL";s:0:"";s:7:"PICTURE";N;s:8:"~PICTURE";N;s:11:"DESCRIPTION";s:0:"";s:12:"~DESCRIPTION";s:0:"";s:16:"DESCRIPTION_TYPE";s:4:"text";s:17:"~DESCRIPTION_TYPE";s:4:"text";s:7:"RSS_TTL";s:2:"24";s:8:"~RSS_TTL";s:2:"24";s:10:"RSS_ACTIVE";s:1:"Y";s:11:"~RSS_ACTIVE";s:1:"Y";s:15:"RSS_FILE_ACTIVE";s:1:"N";s:16:"~RSS_FILE_ACTIVE";s:1:"N";s:14:"RSS_FILE_LIMIT";N;s:15:"~RSS_FILE_LIMIT";N;s:13:"RSS_FILE_DAYS";N;s:14:"~RSS_FILE_DAYS";N;s:17:"RSS_YANDEX_ACTIVE";s:1:"N";s:18:"~RSS_YANDEX_ACTIVE";s:1:"N";s:6:"XML_ID";N;s:7:"~XML_ID";N;s:6:"TMP_ID";N;s:7:"~TMP_ID";N;s:13:"INDEX_ELEMENT";s:1:"Y";s:14:"~INDEX_ELEMENT";s:1:"Y";s:13:"INDEX_SECTION";s:1:"Y";s:14:"~INDEX_SECTION";s:1:"Y";s:8:"WORKFLOW";s:1:"N";s:9:"~WORKFLOW";s:1:"N";s:7:"BIZPROC";s:1:"N";s:8:"~BIZPROC";s:1:"N";s:15:"SECTION_CHOOSER";s:1:"L";s:16:"~SECTION_CHOOSER";s:1:"L";s:9:"LIST_MODE";s:0:"";s:10:"~LIST_MODE";s:0:"";s:11:"RIGHTS_MODE";s:1:"S";s:12:"~RIGHTS_MODE";s:1:"S";s:16:"SECTION_PROPERTY";s:1:"N";s:17:"~SECTION_PROPERTY";s:1:"N";s:14:"PROPERTY_INDEX";s:1:"N";s:15:"~PROPERTY_INDEX";s:1:"N";s:7:"VERSION";s:1:"1";s:8:"~VERSION";s:1:"1";s:17:"LAST_CONV_ELEMENT";s:1:"0";s:18:"~LAST_CONV_ELEMENT";s:1:"0";s:15:"SOCNET_GROUP_ID";N;s:16:"~SOCNET_GROUP_ID";N;s:16:"EDIT_FILE_BEFORE";s:0:"";s:17:"~EDIT_FILE_BEFORE";s:0:"";s:15:"EDIT_FILE_AFTER";s:0:"";s:16:"~EDIT_FILE_AFTER";s:0:"";s:13:"SECTIONS_NAME";s:14:"Разделы";s:14:"~SECTIONS_NAME";s:14:"Разделы";s:12:"SECTION_NAME";s:12:"Раздел";s:13:"~SECTION_NAME";s:12:"Раздел";s:13:"ELEMENTS_NAME";s:16:"Элементы";s:14:"~ELEMENTS_NAME";s:16:"Элементы";s:12:"ELEMENT_NAME";s:14:"Элемент";s:13:"~ELEMENT_NAME";s:14:"Элемент";s:11:"EXTERNAL_ID";N;s:12:"~EXTERNAL_ID";N;s:8:"LANG_DIR";s:1:"/";s:9:"~LANG_DIR";s:1:"/";s:11:"SERVER_NAME";s:31:"подсолнушек-237.рф";s:12:"~SERVER_NAME";s:31:"подсолнушек-237.рф";}s:13:"LIST_PAGE_URL";s:8:"/kinder/";s:14:"~LIST_PAGE_URL";s:8:"/kinder/";s:11:"SECTION_URL";s:0:"";s:7:"SECTION";a:1:{s:4:"PATH";a:0:{}}s:16:"IPROPERTY_VALUES";a:0:{}s:11:"TIMESTAMP_X";s:19:"23.01.2016 14:07:09";}s:18:"templateCachedData";a:5:{s:13:"additionalCSS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/style.css";s:12:"additionalJS";s:62:"/bitrix/components/bitrix/news.detail/templates/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:74:"/bitrix/components/bitrix/news.detail/templates/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;}}}';
return true;
?>