<?
if($INCLUDE_FROM_CACHE!='Y')return false;
$datecreate = '001454244440';
$dateexpire = '001490244440';
$ser_content = 'a:2:{s:7:"CONTENT";s:10636:"		
	    <div class="thumb2"  id="bx_3218110189_22">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/b8f/b8fe6c0481f2b22c8201af39306f2ce8.jpg" alt="Адаптация" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Адаптация                </strong>
              </p>
              <p>
               Как надо готовить родителям ребенка к поступлению в детский сад.&nbsp;&nbsp;              </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=22" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_30">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/f13/f13636062a4794def8916b9974bbd413.jpg" alt="Расчет компенсации родительской платы" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Расчет компенсации родительской платы                </strong>
              </p>
              <p>
               В связи с внесением изменений в постановление Коллегии Администрации Кемеровской области от 30.09.2013 № 410 «О компенсации платы, взимаемой с родителей &#40;законных представителей&#41; за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в образовательных организациях, осуществляющих образовательную деятельность и находящихся на территории Кемеровской области» изменился расчет компенсации платы, взимаемой с родителей &#40;законных представителей&#41; за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в образовательных организациях, осуществляющих образовательную деятельность &#40;далее – компенсация&#41;.              </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=30" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_29">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/628/628de1ea1c8f7ba50e69d24d01522a46.jpg" alt="Правила для родителей" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Правила для родителей                </strong>
              </p>
              <p>
                • Прием детей осуществляется с 7.00 до 8.00 ежедневно, кроме выходных и праздничных дней. Своевременный приход в детский сад - необходимое условие правильной организации воспитательно-образовательного процесса.<br />
 • В отпуск воспитанник уходит&nbsp;&nbsp;на 75 дней, с сохранением места в детском саду. <br />
• Все дети возвращаются с летних каникул&nbsp;&nbsp;со справкой об отсутствии контактов с инфекционными больными и о состоянии здоровья из поликлиники.              </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=29" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_28">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/93b/93b18f55da202c62e2df682db45405bf.jpg" alt="Портрет старшего дошкольника" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Портрет старшего дошкольника                </strong>
              </p>
              <p>
               1. Физически развитый, овладевший основными культурно-гигиеническими навыками. У ребенка сформированы основные физические качества и потребность в двигательной активности.               </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=28" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_26">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/3f1/3f1eff951575172e4770581675e6c7e8.jpg" alt="Материнский капитал" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Материнский капитал                </strong>
              </p>
              <p>
               Доводим до Вашего сведения, что в соответствии с постановлением Правительства Российской Федерации от 24.12.2011 г. № 926 &#40;в редакции постановления Правительства РФ от 14.11.2011 г. № 931&#41; &#40;далее – Правила&#41;, предусмотрена возможность направления средств на оплату содержания ребенка в образовательном учреждении, реализующем основную общеобразовательную программу дошкольного образования.              </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=26" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_25">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/fb9/fb9d214b8c9f2ba93da7d937a8d17807.jpg" alt="Компенсация родительской платы" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Компенсация родительской платы                </strong>
              </p>
              <p>
               о назначении и выплате ежемесячной компенсации части родительской платы за содержание детей в дошкольном образовательном учреждении<br />
<br />
               </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=25" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_24">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/863/863486fe4ae1d01bb3962d27b2faeacd.jpg" alt="Информационная безопасность ребенка" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Информационная безопасность ребенка                </strong>
              </p>
              <p>
               С 1 сентября 2012 года вступил в силу федеральный закон № 436-ФЗ об информационной безопасности детей, который призван защитить подрастающее поколение от медиа-продукции, пропагандирующей наркотические вещества, алкоголь, оправдывающей жестокость и противоправное поведение, отрицающей семейные ценности.              </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=24" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	
		
	    <div class="thumb2"  id="bx_3218110189_23">
          <div class="thumbnail clearfix">
            <figure class="img-polaroid">
              <img src="/upload/iblock/725/725a84191c72bc937ef0dd5fcf751c8f.png" alt="Заявление о назначении компенсации" class="img-responsive">
            </figure>
            <div class="caption">
              <p>
                <strong class="text-uppercase">
                  Заявление о назначении компенсации                </strong>
              </p>
              <p>
               о назначении компенсации платы, взимаемой с родителей &#40;законных представителей&#41; за присмотр и уход за детьми, осваивающими образовательные программы дошкольного образования в образовательных организациях, осуществляющих образовательную деятельность и находящихся на территории Кемеровской области<br />
              </p>
              <a href="/dlya-vas-roditeli/?ELEMENT_ID=23" class="btn-default btn1">Подробнее</a>
            </div>
          </div>
        </div>
	

	<br />
";s:4:"VARS";a:2:{s:8:"arResult";a:8:{s:2:"ID";s:1:"4";s:14:"IBLOCK_TYPE_ID";s:9:"podsolnuh";s:13:"LIST_PAGE_URL";s:45:"#SITE_DIR#/podsolnuh/index.php?ID=#IBLOCK_ID#";s:15:"NAV_CACHED_DATA";N;s:4:"NAME";s:32:"Для Вас, родители!";s:7:"SECTION";b:0;s:8:"ELEMENTS";a:8:{i:0;s:2:"22";i:1;s:2:"30";i:2;s:2:"29";i:3;s:2:"28";i:4;s:2:"26";i:5;s:2:"25";i:6;s:2:"24";i:7;s:2:"23";}s:17:"ITEMS_TIMESTAMP_X";O:25:"Bitrix\\Main\\Type\\DateTime":1:{s:8:"'.chr(0).'*'.chr(0).'value";O:8:"DateTime":3:{s:4:"date";s:26:"2016-01-31 08:01:25.000000";s:13:"timezone_type";i:3;s:8:"timezone";s:13:"Europe/Moscow";}}}s:18:"templateCachedData";a:7:{s:13:"additionalCSS";s:63:"/local/templates/237/components/bitrix/news.list/flat/style.css";s:12:"additionalJS";s:63:"/local/templates/237/components/bitrix/news.list/flat/script.js";s:9:"frameMode";b:1;s:11:"externalCss";a:3:{i:0;s:30:"/bitrix/css/main/bootstrap.css";i:1;s:33:"/bitrix/css/main/font-awesome.css";i:2;s:75:"/local/templates/237/components/bitrix/news.list/flat/themes/blue/style.css";}s:8:"__NavNum";i:1;s:14:"__children_css";a:2:{i:0;s:73:"/bitrix/components/bitrix/system.pagenavigation/templates/round/style.css";i:1;s:73:"/bitrix/components/bitrix/system.pagenavigation/templates/round/style.css";}s:13:"__editButtons";a:16:{i:0;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"22";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:1;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"22";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:2;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"30";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:3;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"30";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:4;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"29";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:5;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"29";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:6;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"28";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:7;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"28";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:8;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"26";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:9;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"26";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:10;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"25";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:11;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"25";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:12;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"24";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:13;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"24";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}i:14;a:5:{i:0;s:13:"AddEditAction";i:1;s:2:"23";i:2;N;i:3;s:31:"Изменить элемент";i:4;a:0:{}}i:15;a:5:{i:0;s:15:"AddDeleteAction";i:1;s:2:"23";i:2;N;i:3;s:29:"Удалить элемент";i:4;a:1:{s:7:"CONFIRM";s:123:"Будет удалена вся информация, связанная с этой записью. Продолжить?";}}}}}}';
return true;
?>