<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
$APPLICATION->SetPageProperty("title", "Подсолнушек №237");
$APPLICATION->SetTitle("Добро пожаловать!");
?> 
  <div class="container">
    <div class="row">
	 <div class="col-sm-12 animated" data-animation="fadeInRight" data-animation-delay="0">
	 <center><h2>ДЕТСКИЙ САД "ПОДСОЛНУШЕК"!</h2></center>
	 </div>
      <div class="col-sm-6 col-sm-push-6 animated" data-animation="fadeInRight" data-animation-delay="0">

        <div class="thumb1">
          <div class="thumbnail clearfix">
                    <p>
          <strong class="text-uppercase">
              наш адрес: 650903, город Кемерово, ж.р.Кедровка</br>
                     улица Греческая деревня ,157 а</br>
                              телефон: 8(342) 69-17-90     </br>         
дрес электронной почты: kem- dsad237@rambler.r         </br>       
  режим работы: пятидневная рабочая неделя, с 7.00 до 19.00</br>
                     </strong>
        </p>
		<a href="/contact/" class="btn-default btn1">Подробнее</a>
          </div>
        </div>

      </div>
      <div class="col-sm-6 col-sm-pull-6 animated" data-animation="fadeInLeft" data-animation-delay="300">

        <p>
          <strong class="text-uppercase">

Наш детский сад "Подсолнушек"</br>
Уютный светлый дом </br>
В Греческой деревне расположен он,</br>
Улыбкой словно солнышко- встречает нас "Подсолнушек",</br>
Занятий здесь не перечесть,</br>
Зарядка, лепка, танцы есть!</br>
Бассейн с чистою водой.</br>
Научит плавать нас с тобой.</br>
Мы будем очень славно жить,</br>
И крепко, крепко все дружить!</br>
          </strong>
        </p> 
 </div>
 	 <div class="col-sm-12 animated" data-animation="fadeInRight" data-animation-delay="600">
<p> Подать заявку в наш детский сад можно на сайте <a href="https://cabinet.ruobr.ru/login/" onclick="return !window.open(this.href)">https://cabinet.ruobr.ru/login/</A></p>
                                                                                                            
<p>На нашем сайте запрещено размещение информации, нарушающей авторские права и законодательство РФ</p>
    </div>
    </div>
  </div>
 </div>

<div id="parallax1" class="parallax">
  <div class="wave2"></div>
  <div class="bg1 parallax-bg"></div>
  <div class="parallax-content">
    <div class="container">
      <div class="txt1 animated" data-animation="zoomIn" data-animation-delay="0">расти вместе с нами</div>
      <div class="txt2 animated" data-animation="zoomIn" data-animation-delay="200">под солнцем!</div>
    </div>
  </div> 
  
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>